<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Breath_Mark
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) : ?>

			<h1 class="entry-title">
				<?php $author = get_field('author');
				$title = get_field('title');
				$year = get_field('year');
				$post_title = get_the_title();
				if(!$title): 
					echo $post_title;
				else:
					echo $title;
					if($year): echo ' (' . $year . ')'; endif;
					if($author): echo '<br/><span>by ' . $author . '</span>'; endif;
				endif; ?>
			</h1>

		<?php else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif; ?>
	</header><!-- .entry-header -->

	<?php // breath_mark_post_thumbnail(); ?>

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php breath_mark_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
