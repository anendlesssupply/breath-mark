<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Breath_Mark
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header screen-reader-text">
				<?php
				the_archive_title( '<h1 class="page-title">', '</h1>' );
				the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->


			<?php 
			$term = get_queried_object();
			$home_gltf = get_field('home_gltf_url', $term);
			$home_3d_object_link = '';
			if($home_gltf): ?>
				<div class="three-item-wrapper">
					<canvas id="three-item" data-gltf="<?php echo $home_gltf; ?>" data-link="<?php echo $home_3d_object_link; ?>"></canvas>
				</div>
			<?php endif; ?>

			<?php
			while ( have_posts() ) :
				the_post();
				$current_id = get_the_ID();
				$thumbnail_id = get_post_thumbnail_id($current_id);
				if($thumbnail_id):
					$mime_type = get_post_mime_type($thumbnail_id);
					if($mime_type == 'image/gif'){
						$image = wp_get_attachment_image_src($thumbnail_id, 'full', false);
					} else {
						$image = wp_get_attachment_image_src($thumbnail_id, 'large', false);
					}
					$src = $image[0];
					$width = $image[1] ?: "100%";
					$height = $image[2] ?: "100%";
					if($src):
						echo '<img aria-hidden="true" class="index-image" data-id="' . $current_id . '" src="' . $src . '" alt="' . get_the_title($current_id) . '" width="' . $width . '" height="' . $height . '" loading="lazy" />';
					endif;
				endif;
			endwhile;

		endif; ?>

	</main><!-- #main -->

<?php
get_footer();
