<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Breath_Mark
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();

			$current_id = get_the_ID();
			$filters = get_the_terms( $current_id, 'reading_cat' );
			$filter_ids = "";
			$prefix = "";
			if($filters):
				foreach($filters as $filter):
					$filter_ids .= $prefix . $filter->term_id;
				endforeach;
			endif;
			
			echo '<span class="data-single" aria-hidden="true" data-id="' . $current_id . '" data-filter-ids="' . $filter_ids . '"></span>';

			get_template_part( 'template-parts/content', get_post_type() );

			get_template_part( 'template-parts/post-navigation' );

		endwhile; // End of the loop.
		?>

	</main><!-- #main -->

<?php
get_footer();
