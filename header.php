<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Breath_Mark
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class('preload'); ?> data-barba="wrapper">
<?php locate_template("assets/svg/symbol-defs.svg", TRUE, TRUE); ?>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'breath-mark' ); ?></a>

	<header id="masthead" class="site-header">
		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle text-button" aria-controls="primary-menu" aria-expanded="false">
				<span class="screen-reader-text"><?php esc_html_e( 'Menu', 'breath-mark' ); ?></span>
				<svg aria-hidden="true" class="icon icon-menu">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-menu"></use>
				</svg>
				<svg aria-hidden="true" class="icon icon-close">
					<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-close"></use>
				</svg>
				<?php bloginfo( 'name' ); ?>
			</button>

			<?php
			/* if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif; */ ?>

			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
					'container'		=> '',
					'fallback_cb'	=> false
				)
			);
			?>
		</nav><!-- #site-navigation -->

		<?php $welcome_text = get_field('header_welcome_text', 'option');
		$more_text = get_field('header_more_text', 'option');
		if( $welcome_text ): ?>
			<div class="site-header-home-text">
				<?php if($more_text):
					echo '<button type="button" class="text-button">' . $welcome_text . '</button>';
				else:
					echo '<p>' . $welcome_text . '</p>';
				endif; ?>
			</div>
			<?php if($more_text): ?>
				<div class="site-header-more-text">
					<button class="text-button site-header-more-text-close" type="button">
						Close	
					</button>
					<div>
						<?php echo $more_text; ?>
					</div>
				</div>
			<?php endif;
		endif; ?>

	</header><!-- #masthead -->


	<?php $filters = get_terms( array(
		'taxonomy' => 'reading_cat',
		'hide_empty' => false,
	) );
	if($filters): ?>
		<aside class="site-reading-room-filters">
			<ul>
				<?php foreach($filters as $filter): 
					$filter_id = $filter->term_id;
					$link = get_term_link($filter_id, 'reading_cat');
					if($link): 
						$connected_post_ids = '';
						$prefix = '';
						$args = array(
							'post_type' => 'post',
							'tax_query' => array(
								array(
									'taxonomy' => 'reading_cat',
									'field' => 'term_id',
									'terms' => $filter_id
								)
							),
							'fields' => 'ids'
						);
						$tax_query = new WP_Query( $args );
						if ( $tax_query->have_posts() ) :
							while ( $tax_query->have_posts() ) :
								$tax_query->the_post();
								$connected_post_ids .= $prefix . $post;
								$prefix = ",";
							endwhile;
						endif;
						wp_reset_postdata();
						?>
						<li data-filter-id="<?php echo $filter_id; ?>" data-ids="<?php echo $connected_post_ids; ?>"><a href="<?php echo $link; ?>" title="View all <?php echo $filter->name; ?> entries"><?php echo $filter->name; ?></a></li>
					<?php endif;
				endforeach; ?>
			</ul>
		</aside>
	<?php endif; ?>

	<?php $args = array(
		'post_type' => 'post',
		'posts_per_page' => -1
	);
	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() ): ?>
		<div class="site-reading-room-index">
			<ul>
				<?php while ( $the_query->have_posts() ):
					$the_query->the_post(); ?>
					<li data-id="<?php echo get_the_ID(); ?>">
						<a href="<?php the_permalink(); ?>">
							<?php $author = get_field('author');
							$title = get_field('title');
							$year = get_field('year');
							$post_title = get_the_title();
							if(!$title): 
								echo $post_title;
							else:
								echo '<em>' . $title . '</em>';
								if($year): echo ' (' . $year . ')'; endif;
								if($author): echo '<br/>by ' . $author; endif;
							endif; ?>
						</a>
					</li>
				<?php endwhile; ?>
			</ul>
		</div>
	<?php endif;
	wp_reset_postdata(); ?>

	<div class="scroll-to-top-observer" aria-hidden="true"></div>

	<div class="site-container" data-barba="container" data-class="<?php echo join( ' ', get_body_class( ) ); ?>" data-barba-namespace="<?php echo get_the_title(); ?>">