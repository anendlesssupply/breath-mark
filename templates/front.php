<?php
/**
 * Template Name: Front Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Fulfilment_Services_Ltd
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			
			$home_gltf = get_field('home_gltf_url', 'option');
			$home_3d_object_link = get_field('home_3d_object_link', 'option') ?: '';
			if($home_gltf): ?>
				<div class="three-item-wrapper">
					<canvas id="three-item" data-gltf="<?php echo $home_gltf; ?>" data-link="<?php echo $home_3d_object_link; ?>"></canvas>
				</div>
			<?php endif; ?>

		<?php endwhile; // End of the loop.
		?>

		<?php $args = array(
			'post_type' => 'post',
			'posts_per_page' => -1,
			'fields' => 'ids'
		);
		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ):
			while ( $the_query->have_posts() ):
				$the_query->the_post();
				$current_id = $post;
				$thumbnail_id = get_post_thumbnail_id($current_id);
				if($thumbnail_id):
					$mime_type = get_post_mime_type($thumbnail_id);
					if($mime_type == 'image/gif'){
						$image = wp_get_attachment_image_src($thumbnail_id, 'full', false);
					} else {
						$image = wp_get_attachment_image_src($thumbnail_id, 'large', false);
					}
					$src = $image[0];
					$width = $image[1] ?: "100%";
					$height = $image[2] ?: "100%";
					if($src):
						echo '<img aria-hidden="true" class="index-image" data-id="' . $current_id . '" src="' . $src . '" alt="' . get_the_title($current_id) . '" width="' . $width . '" height="' . $height . '" loading="lazy" />';
					endif;
				endif;
			endwhile;
		endif;
		wp_reset_postdata(); ?>


	</main><!-- #main -->

<?php
get_footer();
