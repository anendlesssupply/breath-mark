<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Breath_Mark
 */

?>

    </div><!-- barba -->
    <div class="site-footer">
        <button class="scroll-to-top-button">
            <span class="screen-reader-text">Back to top</span>
            <svg viewBox="0 0 20 10" aria-hidden="true">
                <polygon points="10,0 20,10 0,10" />
            </svg>
        </button>
    </div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
